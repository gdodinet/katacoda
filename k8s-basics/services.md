### Definition

Services are defined as "an abstract way to expose an application running on a set of Pods as a network service".

We just saw that pods are not durable entities. While a pod gets its own IP address, you can't just rely on the information to route traffic as pods come and go and can be killed.

To solve this problem, services are introduced. A service groups together a logical set of pods that can be accessed through the service. The service balances the load between the different backends. 

![High level picture](/gdodinet/scenarios/k8s-basics/assets/k8s.services.png)

### Tasks 

##### Create a simple pod

1. To illustrate these concepts, let's start by creating a pod named *webserver* that declares a single *nginx* container. 

`kubectl run webserver --restart=Never --image=nginx`{{execute}}

2. Wait until the pod is in **Running** state. 

`kubectl get po webserver -w`{{execute}}

3. Ssh into the nginx container and check that nginx is listening on port 80. You can use a simple curl command but will need to install it.

`kubectl exec webserver -- /bin/sh -c "apt update && apt install -y curl && curl http://localhost:80/"`{{execute}}
  
>>What do you see?<<
(*) The nginx welcome page
() A 404 error page
() Nothing. Webserver is not running 


##### Expose the pod

4. Now we can *expose* the pod, i.e. create a service. We want to expose the pod on the port 90.

`kubectl expose pod/webserver --port=90 --target-port=80`{{execute}}

5. List the available services. 

`kubectl get service`{{execute}}

6. Output the service as yaml

`kubectl get svc webserver -o yaml`{{execute}}

>>What is the type of the service you created?<<
() LoadBalancer
(*) ClusterIP
() NodePort

##### Access the service

A service of type ClusterIP (default) exposes a set of pods *internally*: it is accessible from pods running inside the same cluster. It is assigned a (virtual) IP and its name resolves to that IP.

>>Note the Internal IP of the service<<
=~= 10.

7. You can test that the pod is exposed under your service name by accessing it from a temporary pod:

`kubectl run volatile-pod --restart=Never --image=busybox --rm -it -- /bin/sh`{{execute}}

```
# wget -O- http://webserver:90/ | cat -
# wget -O- http://<SVC_INTERNAL_IP>:90/ | cat -
# exit 
```

Notice that you the nginx server is accessible under both the service internal IP and its name (webserver).

*Note*: if you list the pods, you can notice that the *volatile-pod* pod has indeed been deleted.  

Delete the service:

`kubectl delete svc webserver`{{execute}}


##### Expose a deployment

8. Create a deployment named *webserver* with 3 replicas and an image *nginx*. 

`kubectl run webserver --replicas=3 --image=nginx`{{execute}}

9. Create a service to expose this deployment:

`kubectl expose deploy/webserver --port=8080 --target-port=80`{{execute}}

10. Check that service is correctly configured. 

11. How can you make sure that the service balance between the different pods?

Explore the other types of services (see references below). 

### References

https://kubernetes.io/docs/concepts/services-networking/service/
https://medium.com/google-cloud/kubernetes-nodeport-vs-loadbalancer-vs-ingress-when-should-i-use-what-922f010849e0

