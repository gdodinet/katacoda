
### Definition

A Deployment provides declarative updates for Pods and ReplicaSets.

You describe a desired state in a Deployment, and the Deployment Controller changes the actual state to the desired state at a controlled rate. 

What does a desired state mean? You can see a desired state as a collection of requirements. Example:
 
- we have a *webapp*; 
- we balance the load between *2 instances*; 
- we *allocate 500Mi* to each instance; 
- an instance takes about *30 seconds to be ready*  

The deployment controller ensures that those requirements get honored.

### Tasks 

##### Basics

1. Create a deployment named *nginx-1* from image *nginx:latest*

`kubectl run nginx-1 --restart=Always --image=nginx:latest`{{execute}}

Or:

`kubectl create deploy nginx-1 --image=nginx:latest`{{execute}}

2. List the deployments in the current namespace

`kubectl get deploy`{{execute}}

3. List the pods in the current namespace

`kubectl get pods`{{execute}}

What do you observe?

##### ReplicaSets

A Deployment owns *ReplicaSet* objects. A ReplicaSet’s purpose is to maintain a stable set of replica Pods running at any given time. Consider that Deployment is an abstraction over a ReplicaSet.

4. List the ReplicaSet in the current namespace

`kubectl get rs`{{execute}}

What do you notice?

5. Delete the pod attached to the deployment you created earlier

If you created the deployment with "kubectl create":

`kubectl delete pod $(kubectl get po -l=app=nginx-1 | awk 'NR>1 {print $1}')`{{execute}}

If you created the deployment with "kubectl run":

`kubectl delete pod $(kubectl get po -l=run=nginx-1 | awk 'NR>1 {print $1}')`{{execute}}

6. List the pods in the current namespace. What do you notice?

##### Scale your application

Your application is being popular and you can't handle the load with one pod only and you need to scale up.

7. Update the deployment *nginx-1* so that it now controls more pods.

`kubectl scale deploy nginx-1 --replicas=2`{{execute}}

8. List the pods in the current namespace

>>How many nginx pods do you see<<
() 1
(*) 2
() 3
 
9. List the deployments in the current namespace

What do you notice?

*Note* You can also configure an autoscaling strategy by declaring HorizontalPodAutoscaler resources

### References

https://kubernetes.io/docs/concepts/workloads/controllers/deployment/
https://kubernetes.io/docs/concepts/workloads/controllers/replicaset/
https://kubernetes.io/docs/tasks/run-application/horizontal-pod-autoscale/
