### Definition

Pods are the smallest deployable units of computing that can be created and managed in Kubernetes.

### In layman terms

A pod is a group of one or more containers. In its most basic form, a pod declares a single container. Think of this container as an application (f.i. a webapp).

**Note:** pods are **not**  durable entities: if they're evicted (f.i. because there's not enough resources), they won't be automatically recreated. 

### Tasks 

##### Basics

1. Create a pod named *hello-world-1* that declares a *busybox* container. The container outputs "*Hello World*" and exits.

`kubectl run hello-world-1 --restart=Never --image=busybox -- echo 'Hello World'`{{execute}}

"kubectl run" uses generators to create persistent objects. The type of resource created can be infered by the specified restart policy. 

Kubernetes allows to interact with resources using imperative commands as above or declaratively, using configuration files (or manifests). If the former 
allows for quick experimentation, the latter is prefered when working with reproducible workloads.

You can create the pod *hello-world-1* declaratively by using the *apply* command: 

```
(cat <<EOF 
apiVersion: v1
kind: Pod
metadata:
  labels:
    run: hello-world-1
  name: hello-world-1
spec:
  containers:
  - args:
    - /bin/sh
    - -c
    - echo "Hello World"
    image: busybox
    name: hello-world-1
EOF
) | kubectl apply -f -
```{{execute}}


2. List the pods in the current namespace (*default*)

`kubectl get pod -n default`{{execute}}

>>How many pods are running?<<
(*) 0
() 1
() 2

3. Retrieve the yaml definition of the pods

`kubectl get pod hello-world-1 -o yaml`{{execute}}

What do you observe?

##### Observability 

1. Create a pod named *hello-world-2* that declares a *busybox* container. The container outputs "*Hello World 2*" and sleeps for 1 day. You may want to use this command to run the container:

``` 
/bin/sh -c "echo 'Hello World 2' && sleep 1d"
```

2. List the pods in the current namespace

>>How many pods are running?<<
() 0
(*) 1
() 2

3. Get the logs of the *hello-world-2* pod

`kubectl logs hello-world-2`{{execute}}

4. Get the logs of the *hello-world-1* pod. 

##### Connecting to a pod

1. Connect to the pod *hello-world-2* and list the environment variables

`kubectl exec hello-world-2 -it -- /bin/sh`{{execute}}

```
# env
# exit
```

Or:

`kubectl exec hello-world-2 -it -- env`{{execute}}

2. Connect to the pod *hello-world-1* and list the environment variables. 

>>What do you observe?<<
[] The environment variables are the same as hello-world-2
[] Cannot get the environment variables because the pod is in error
[*] Cannot get the environment variables because is not running

3. Retrieve the state of the pod *hello-world-1* 

`kubectl describe po hello-world-1`{{execute}}

>>what is the status of the pod<<

() Pending
() Running 
() Failed
() Succeeded
(*) Terminated
() Completed
() Unknown 

##### Clean up the environment

1. Delete the newly created pods 

`kubectl delete pod hello-world-{1..2}`{{execute}}

Or:

`kubectl delete pod hello-world-1 hello-world-2`{{execute}}

### References

https://kubernetes.io/docs/concepts/workloads/pods/pod/
https://kubernetes.io/docs/concepts/workloads/pods/pod-lifecycle/
