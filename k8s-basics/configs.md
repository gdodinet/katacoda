### Definition

ConfigMaps hold persistent key-value pairs of configuration data. By allowing to externalize configuration they provide a way to decouple an application from its configuration and allow to keep a containerized application portable.

### Tasks 

##### Create a simple ConfigMap

1. We just saw that, at its core, a ConfigMap is merely a key-value map. It is fairly explicit when you consider its yaml representation. Let's start by creating a *hello-world* ConfigMap using the declarative approach.

```
(cat <<EOF
kind: ConfigMap
apiVersion: v1
metadata:
  name: hello-world
data: 
  greetings: Hello
  name: World
EOF
) | kubectl apply -f -
```{{execute}}

2. List the ConfigMaps in the current namespace to check that the ConfigMap has been created.

`kubectl get cm`{{execute}}

3. Create a ConfigMap using the imperative approach

`kubectl create cm hello-world-2 --from-literal=greetings=hello --from-literal=name=world`{{execute}}

We can check that ConfgMaps *hello-world* and *hello-world-2* holds the same data:

`kubectl get cm hello-world -o jsonpath='{.data}{"\n"}'`{{execute}}

`kubectl get cm hello-world-2 -o jsonpath='{.data}{"\n"}'`{{execute}}

>>Both commands return:<<
=~= greetings:hello name:world

##### Use a ConfigMap to configure an application

Now that you have created a configmap, you can use to configure your application. 

4. Get the yaml representation of a pod named *hello-world* that uses a *busybox* container that prints a hello world message based on the value of environment variables. Don't create it just yet.

`kubectl run hello-world --restart=Never --dry-run --image=busybox -o yaml > hello-world.yaml`{{execute}} 

5. Edit the pod configuration to use the ConfigMap data as environemnt variables for the container busybox. Make it sleep for one day.

`nano hello-world.yaml`{{execute}}

```̀
# configure the container with the following bits:
args:
- /bin/sh
- -c
- echo "$greetings $name!" && sleep 1d
envFrom:
- configMapRef:
    name: hello-world
```

Save the file (^O), close the editor (^X) and create the pod.

`kubectl apply -f hello-world.yaml`{{execute}}

6. Check that there's an environment variable named *greetings*

`(kubectl exec hello-world -it -- env) | grep greetings`{{execute}}

Check the pod is running using 

`kubectl get po hello-world`

>>what flag would you wait until the pod status is Running<<
() -f
() -g
(*) -w

>>What is the logs of the hello-world pod?<<
=== Hello World!


### More about configuration

We've just tackled the surface of the configuration issue. You can find more information on kubernetes.io regarding ConfigMaps (and Secrets), f.i.: 
- How to create ConfigMaps and Secrets from files
- How to declare specific keys
- How to bind CM and Secrets to Volumes (but what is a Volume, say?)
- etc.
