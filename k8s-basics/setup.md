### Before you begin

This scenario uses Minikube. Minikube is a tool that makes it easy to run Kubernetes locally. Minikube runs a single-node Kubernetes cluster inside a Virtual Machine.

First, start by checking that Minikube is correctly installed:

`minikube version`{{execute}}

That should be case. If so, you can start Minikube:

`minikube start --wait=false`{{execute}}

Minikube takes up a couple minutes to boot up. Once Minikube is started, you can continue to the next step. 

### References

https://kubernetes.io/docs/setup/learning-environment/minikube
